This is a summary of the most relevant changes. For a complete list of
changes see https://forge.physik.rwth-aachen.de/projects/pxl

pxl 3.5.0, important changes since 3.4.0
------------------------------------------------------------------

	* fix Variant toFloat/toDouble for boolean
	* Allow numpy arrays as alien arrays in BasicNVector
	* Add rapidity functions to LorentzVector, extend test_core.cc.
	* use class parameter in PyModule to find class
	* upgrades gtest to latest version 1.7.0 to be compatible with OS X 10.9
	* modifies Python.cmake to make it compatible with OS X 10.9
	* Improve error message on wrong python version
	* Remove obsolete usage of pkgconfig in FindPXL
	* Added option for select module isntall path in FindPXL
	* UserRecord now throws runtime_error if non-exisitng entry is erased. This is now consistent with other methods. Closes: #1240
	* Add support for multiple numpy floats. Closes: #1219
	* make all modules available in python
	* add onPublish callback to pymodule
	* fix CMAKE python lib finding
	* add publish/subscribe to analysis


pxl 3.4.0, important changes since 3.3.3
------------------------------------------------------------------

	* Install pkg-config files again, Closes: #1007
	* check for pkgconfig files, fixes #913
	* Improved debug output
	* Check for non string returns in PyModuleModule. Closes: #732.
	* Throw exception in InputFile if open non existing file. Closes: #852
	* Added cmake option to disbale build of python docstrings
	* clears existing value vector before adding new values when setting module option of type vector of strings (fixes vispa ticket #943)
	* Added test for correct removal on connections in analysis if module is removed.
	* Remove status messages on reload of script in PyModule
	* Return ./ as parent directory instead of empty string, Closes: #892
	* Make SoftRelations between two objects of same name unique, Closes: #902
	* Fix treatment of start and end index in output file; closes #398 and #848
	* fixes #893, allow empty source names in PyModules
	* Fix Script::objectToString for python <2.7
	* fix Doxygen generation for Variant
	* add mission to<std::string>, fix #889
	* fixes #810 (adds convenience methods for Relative: getDaughter() and getMothers()
	* Fix auto decay reconstruction
	* Not interpreting PyObject as unicode in Script::objectToString, Closes: #875, #872
	* CMake throws fatal error if pytohn not found, Closes: #858
	* Added Fisher distribution of vectors on sphere to random
	* Return tuple in pythonwrapping of HealpixMap::getPixelsInCone,Closes: #729
	* Added missing clone method to healpixmap, Closes: #744
	* randNorm default: variance = 1 instead of 0 (standard normal distribution)
	* added index=-1 to randPowerLaw and randBrokenPowerLaw
	* Fixed FTBFS if missing dcap due to double .cc in CmakeLists
	* refactor variant, improve usage
	* fix windows build
	* add sftp file
	* introduce abstract file class
	* improve Stream, add Stream tests
	* fix -builtin use on older SWIG versions
	* make swig interfaces backward compatible (1.3.29)
	* refactor i/o stream
	
pxl 3.3.3, important changes since 3.3.2
------------------------------------------------------------------

	* fix dcache file implementation

pxl 3.3.2, important changes since 3.3.1
------------------------------------------------------------------

	* fix installation of pxl-doxy.i

pxl 3.3.1, important changes since 3.3.0
------------------------------------------------------------------

	* fix swig 2.0.9 bug
	* fix swig interfaces for all versions
	* update bundled expat version to 2.1.0
	* update bundled zlib version to 1.2.8
	* fix pxlrc crash on windows

pxl 3.3.0, important changes since 3.2.0
------------------------------------------------------------------

	* Added numpy array interface to BasicNVector and BasicMatrix.
	* Added USAGE_TEXT_SELECTION to modules, Closes: #405
	* Add setVector and addVector methods to particle to increase consistency with getVector
	* Do not execute beginJob, endJob, beginRun, endRun on disabled modules. Closes: #1079
	* Fixed Native_toVariant conversion for type(value)==long, Closes: #1026
	* Remove non-existing object from BasicContainer throws exception. Closes: #856
	* Added function to expand enviroment variables
	* Use expansion of enviroment variables in analyis find file and outpmodule. Closes: #1041.
	* Remove empty strings from python search path to avoid security issue
	* Add path of python module to local python search path. Closes: #1037
	* moved skeletons from Vispa to pxl, Closes: #1097
	* Write analysis to tmpfile first and copy on close to avoid corrupted xml files. Closes: #1070
	* Allow python list of variant-able data types in userrecord. Closes #1089
	* Added missing DLL_EXPORT. Should fix windows builds
	* Do not require pkgcfg
	* Print error summary on finish of pxlrun
	* Colored ConsoleHandler
	* Add getDataPath option to pxlrun
	* Use pxlrun instread of pkg-config to provide info on user plugin dir
	* Added conveniance macro to insll pxl modules to FindPXL
	* Add more informations about the pxl installation to pxlrun, and use this in FindPXL
	* Add RootSerializable
	* Add Zmumu example with efficiency correction using a RootSerializable
	* Added missing functionality python interface of SoftRelations
	* Added method to remove objects of type from BasicContainer, Closes: #1153
	* Improved error message on missing python modules. Closes #1150
	* Understand numpy datatypes in userRecord, Closes: #1126
	* Add option to add additional directories to search path in pxlrun, Closes: #1168
	* Added pxlconfiguration file, Closes: #1169
	* add system wide pxlrc, PXL_SYSTEM_RC compiler flag
	* use console colors only when tty is present -> no colors in files.
	* Removed check on compression mode in ChunkWriter
	* move autodecayreconstructor from pxl into own package (fixes #1248)
	* remove neutrino pz solutions and move them to auto decay reconstruction module
	* Added example for user defined types
	* Add reference-based deltaX methods to LorentzVector and Basic3Vector, which should significantly simplify C++ code and avoid statemens such as fv1.deltaR(&fv2)
	* Reset number of objects after file section has been written
	* Allow compression level of zero
	* Install swig .i files needed for user defined data types, like root serializable
	* Fixed healpix order 0, Closes: #1280
	* make loadDefaultPlugins available from within python, Closes: #1250


pxl 3.2.0, important changes since 3.1.0
------------------------------------------------------------------

	* Install pkg-config files again, Closes: #1007
	* check for pkgconfig files, fixes #913
	* Improved debug output
	* Check for non string returns in PyModuleModule. Closes: #732.
	* Throw exception in InputFile if open non existing file. Closes: #852
	* Added cmake option to disbale build of python docstrings
	* clears existing value vector before adding new values when setting module option of type vector of strings (fixes vispa ticket #943)
	* Added test for correct removal on connections in analysis if module is removed.
	* Remove status messages on reload of script in PyModule
	* Return ./ as parent directory instead of empty string, Closes: #892
	* Make SoftRelations between two objects of same name unique, Closes: #902
	* Fix treatment of start and end index in output file; closes #398 and #848
	* fixes #893, allow empty source names in PyModules
	* Fix Script::objectToString for python <2.7
	* fix Doxygen generation for Variant
	* add mission to<std::string>, fix #889
	* fixes #810 (adds convenience methods for Relative: getDaughter() and getMothers()
	* Fix auto decay reconstruction
	* Not interpreting PyObject as unicode in Script::objectToString, Closes: #875, #872
	* CMake throws fatal error if pytohn not found, Closes: #858
	* Added Fisher distribution of vectors on sphere to random
	* Return tuple in pythonwrapping of HealpixMap::getPixelsInCone,Closes: #729
	* Added missing clone method to healpixmap, Closes: #744
	* randNorm default: variance = 1 instead of 0 (standard normal distribution)
	* added index=-1 to randPowerLaw and randBrokenPowerLaw
	* Fixed FTBFS if missing dcap due to double .cc in CmakeLists
	* refactor variant, improve usage
	* fix windows build
	* add sftp file
	* introduce abstract file class
	* improve Stream, add Stream tests
	* fix -builtin use on older SWIG versions
	* make swig interfaces backward compatible (1.3.29)
	* refactor i/o stream

pxl 3.1.0, important changes since 3.0.3
------------------------------------------------------------------

	* Throw exception when write operation fails (Closes: #694)
	* fix order of input files #718
	* bring variant into defined state
	* use Variant inside Module for Options.
	* Fixed angle calculation in Basic3Vector for cases of parallel and antiparallel vectors
	* Astro: Normalize vector in set setGalacticDirectionVector, Closes: #666
	* Increased safety in usage of alien arrays in BasicNVector, Closes: #651
	* Make BasicContainer python iterable, Closes: #714
	* Return const ref instead of const pointer to modules and connection lists in analysis
	* Fix pypxl on swig2.0
	* fixed default build type to Release
	* fixed cmake python search on mingw
	* Fixed setting of string vector analysis, Closes: #655
	* fix memory leak in scripting loader
	* Fixed memory leak in BasicNVector
	* Fixed potential memory leak in analysis

pxl 3.0.3, important changes since 3.0
------------------------------------------------------------------

	* Fixed potential memory leak in BasicNVector and BasicMatrix
	* fixed OutputModule
	* Removed potential error hiding NULL pointer check in AstroObject
	* Fixed memory leak in input module and added warning for empty filenames
	* Fixed angular distance floating point precision for rare 	circumstances
	* Possible fix for angular distance floating point precision on mac
	* Fixed angular distance calculation on mac
	* added BasicNVector.norm(L)
	* add check if option is available when getting option values
	* Fixed pypxl on SL5
	* Potential fix for workaround swig::stop_iteration, restores python default dlopen behaviour
	* Don't allow PyModules without python code
	* Improved python bindings of healpixmap, python style getNeighbors
	* Fixed python bindings of HealpixMap::getNumberOfPixels
	* uniformDistributionINHealpix
	* Improved seperation of healpix due to license issues, added option USE_HEALPIX
	* improved unzip performance
	* AstroBasicObject has default direction
	* REmoved buggy random direction in pixel from healpixmap:
	* Updated Variant. Can store a vector of variants now
	* Fixed __getitem__, __setitem__ in python wrap of BasicMatrix
	* Improved python interface BasicMatrix, gives correct shape and len
	* Module::setOption throws exception if option not added first. Closes 	#401
	* Added XYZ setters for SGC and Equatorial Coordinates
	* Added norm to Basic3Vector, Closes: #678
	* Added getAngleTo to Basic3Vector
	* Added rotate to Basic3Vector

pxl 3.0.0, 21.06.10 
------------------------------------------------------------------
 * API Changes:
   The complete Api has been reviewed and adjusted. For a complete list
	 of changes see the changelog http://pxl.hg.sourceforge.net/hgweb/pxl/pxl/log,
	 or the doxygen documentation
	 
	core/ base&io:
		* base and io have been merged into core
		BasicContainer 
		* Added typedef std::map<std::string, Serializable*> ContainerIndex
		* Changed return type inline const map_t& getIndexEntry() const -> inline const ContainerIndex& getIndex() const
		Event
		* Renamed setObject -> insertObject
		Filter
		* Renamed Comparator -> ComparatorInterface
		* Renamed FilterCriterion - > FilterCriterionInterface
		LorentzVector
	  * Removed inline void setMass(double m)
		Object
	  * Renamed inline bool getLocked() -> inline bool isLocked() const
		* Renamed inline int getWorkflag() -> inline int getWorkFlag()
		* Renamed inline void setWorkflag(int v) -> inline void setWorkFlag(int v)
		UserRecord 
	  * Renamed UserRecord -> UserRecords
	
	astro:
	 AstroBasicObject
		* Removed of setZenith and setAzimuth method
		* Renamed setLongitudeLatitude -> setGalacticLongitudeLatitude
	
	hep:
	 Collision:
		*  Removed typedef weak_ptr<Collision> CollisionWkPtr
	 Particle:
		* Renamed getParticleId() -> getPdgNumber()
		* Renamed setParticleId() -> setPdgNumber()
		* Renamed setVector() -> getVector()
	 AnalysisFork:
		* Changed parameters: virtual void beginJob(const ObjectOwner* input = 0) -> virtual void beginJob(const Serializable* input = 0) 
		* Changed parameters: virtual void beginRun(const ObjectOwner* input = 0) -> virtual void beginRun(const Serializable* input = 0) 
		* Changed parameters: virtual void endJob(const ObjectOwner* input = 0) -> virtual void endJob(const Serializable* input = 0) 
		* Changed parameters: virtual void endRun(const ObjectOwner* input = 0) -> virtual void endRun(const Serializable* input = 0) 
		* Changed parameters: virtual void finishEvent(const ObjectOwner* input = 0) -> virtual void finishEvent(const Event* event = 0)
		* Changed parameters: virtual void analyseEvent(const ObjectOwner*
		* input = 0) -> virtual void analyseEvent(const Event* event = 0)

 * Starting with pxl 3.0.0 the buildsystem is cmake 
 
 * Mechanism to include user classes in pxlrun, Closes: #152
 * Introduction of logging system, Closes: #105
 * python scripts are now classes, Closes: #137, #138
 * pxlrun can execute multiple analysis, one after the other, Closes: #210
 * pypxl returns objects of correct type, Closes: #195, #184, #185
 * Method to store meta information in analysis xml file, Closes: #207
 * Support for astronomical coordinate systems, Closes: #202, #203, #204
 * Random generator is now availeable from python, Closes: #181
 * Fixed operator overloading in python, Closes: #160, #100
 * Userrecord can carry serializables, Closes: 
 * Introduced Analysis Class, Closes: #148
 * Make InputFile.close() save if not InputFile.good(), Closes: #132
 * In layout of orphans, increase distance to borders, Closes: #113
 * Introduced N Dimensional Vector and Matrix, Closes: #101, #102
 * Make ad-hoc AutoLayout work for several large decay trees, Closes: #98
 * Create and submit ILC filler, Closes: #18
 * design meta info for scriptfilter modules, Closes: #31
 * pxlrun can change module parameters via cmdline, Closes: #146
 * Remove layout from particle, Closes: #175
 * Document Id, InformationChunk, Layout, Relations, Closes: #176
 * Version information in pxl.hh, Closes: #144





